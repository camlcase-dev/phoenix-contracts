```bash
# :: 'p : mutez : contract 'p : 'S   ->   operation : 'S
# set limit entry point
# transfer tokens to user each time an entrypoint is called

tezos-client --addr 178.62.73.193 originate contract aliceLimit transferring 0 from alice running ./spending_limit.tz --init 'Pair (Pair None None) "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' --burn-cap 5 --force

tezos-client --addr 178.62.73.193 get balance for alice

# give limit some FA1.2

tezos-client --addr 178.62.73.193 list known contracts | grep alice
tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn

tezos-client --addr 178.62.73.193 list known contracts | grep tezosGold
KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2

tezos-client --addr 178.62.73.193 list known contracts | grep aliceLimit
KT1UPdnaLVzYCiBvi2XqrziUVbMYj5BrsR3Q

tezos-client --addr 178.62.73.193 list known contracts | grep tezosGoldExchange
KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4

tezos-client --addr 178.62.73.193 transfer 0 from alice to tezosGold --entrypoint 'transfer' --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "KT1UPdnaLVzYCiBvi2XqrziUVbMYj5BrsR3Q") 1000' --burn-cap 1

tezos-client --addr 178.62.73.193 get big map value for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' of type 'address' in tezosGold
tezos-client --addr 178.62.73.193 get big map value for '"KT1UPdnaLVzYCiBvi2XqrziUVbMYj5BrsR3Q"' of type 'address' in tezosGold

# give the limit some money
tezos-client --addr 178.62.73.193 transfer 10 from alice to aliceLimit --entrypoint 'default' --arg 'Unit'

# set limit to 5 xtz
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'setLimit' --arg 'Some 5000000' --burn-cap 1

# send FA1.2 via limit

tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2"; CONTRACT %transfer (pair (pair address address) nat); IF_SOME {PUSH mutez 0; PUSH nat 100; PUSH address "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"; PUSH address "KT1UPdnaLVzYCiBvi2XqrziUVbMYj5BrsR3Q"; PAIR; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}' --dry-run

tezos-client --addr 178.62.73.193 get script storage for aliceLimit

# TRANSFER_TOKENS
# :: 'p : mutez : contract 'p : 'S   ->   operation : 'S

# FA1.2
# pair %transfer (pair address address) nat

# give alice's tezos gold to aliceLimit
tezos-client --addr 178.62.73.193 transfer 0 from alice to tezosGold --entrypoint 'transfer' --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "KT1UPdnaLVzYCiBvi2XqrziUVbMYj5BrsR3Q") 1000' --burn-cap 1

# Approve TezosGoldExchange to use AliceLimit's tez
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2"; CONTRACT %approve (pair address nat); IF_SOME {PUSH mutez 0; PUSH nat 100; PUSH address "KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4"; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}' --burn-cap 1

# Add Liquidity
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4"; CONTRACT %addLiquidity (pair (pair address nat) (pair nat timestamp)); IF_SOME {PUSH mutez 1000000; PUSH timestamp "2020-12-30T18:00:00Z"; PUSH nat 100; PAIR; PUSH nat 1; PUSH address "KT1WpcWd6xfdNDTZwrEZJm1VznC3GiGF6ZZN"; PAIR; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}'

# XTZ to Token
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4"; CONTRACT %xtzToToken (pair (pair address nat) timestamp); IF_SOME {PUSH mutez 1000000; PUSH timestamp "2020-12-30T18:00:00Z"; PUSH nat 1; PUSH address "KT1UPdnaLVzYCiBvi2XqrziUVbMYj5BrsR3Q"; PAIR; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}'

# Token to XTZ
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4"; CONTRACT %tokenToXtz (pair (pair (pair address address) (pair nat mutez)) timestamp); IF_SOME {PUSH mutez 0; PUSH timestamp "2020-12-30T18:00:00Z"; PUSH mutez 1000000; PUSH nat 50; PAIR; PUSH address "KT1UPdnaLVzYCiBvi2XqrziUVbMYj5BrsR3Q"; PUSH address "KT1UPdnaLVzYCiBvi2XqrziUVbMYj5BrsR3Q"; PAIR; PAIR; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}'


const owner         : address; // the address of the owner of FA1.2
                      const to_           : address;
                      const tokens_sold   : nat;
                      const min_xtz_bought: tez;
                      const deadline      : timestamp;
                      var store           : store):                      

pair %tokenToXtz
                   (pair (pair (address %0) (address %1)) (pair (nat %2) (mutez %3)))
```
