Useful command line commands for developing spending_limit_reference.ligo.

```bash
ligo compile-contract spending_limit_reference.ligo main > spending_limit_reference_raw.tz

tezos-client --addr 178.62.73.193 originate contract aliceLimit transferring 0 from alice running ./spending_limit_reference.tz --init 'Pair (Pair None None) "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' --burn-cap 5 --force --dry-run

tezos-client --addr 178.62.73.193 list known contracts | grep tezosGold
KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2

tezos-client --addr 178.62.73.193 list known contracts | grep aliceLimit
KT1WpcWd6xfdNDTZwrEZJm1VznC3GiGF6ZZN

tezos-client --addr 178.62.73.193 transfer 10 from alice to aliceLimit --entrypoint 'default' --arg 'Unit' --burn-cap 1

tezos-client --addr 178.62.73.193 get balance for alice
tezos-client --addr 178.62.73.193 get balance for aliceLimit

tezos-client --addr 178.62.73.193 transfer 0 from alice to tezosGold --entrypoint 'transfer' --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "KT1WpcWd6xfdNDTZwrEZJm1VznC3GiGF6ZZN") 1000' --burn-cap 1

# %approve pair address nat
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2"; CONTRACT %approve (pair address nat); IF_SOME {PUSH mutez 0; PUSH nat 100; PUSH address "KT1WpcWd6xfdNDTZwrEZJm1VznC3GiGF6ZZN"; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}' --burn-cap 1

tezos-client --addr 178.62.73.193 get big map value for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' of type 'address' in tezosGold

tezos-client --addr 178.62.73.193 get big map value for '"KT1WpcWd6xfdNDTZwrEZJm1VznC3GiGF6ZZN"' of type 'address' in tezosGold

tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2"; CONTRACT %transfer (pair (pair address address) nat); IF_SOME {PUSH mutez 0; PUSH nat 100; PUSH address "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"; PUSH address "KT1WpcWd6xfdNDTZwrEZJm1VznC3GiGF6ZZN"; PAIR; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}'

tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'setLimit' --arg '1000000'
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'setLimit' --arg 'None'

# send 1 xtz from aliceLimit to alice
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; SOURCE; CONTRACT unit; IF_SOME {PUSH mutez 1000000; UNIT; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;};}' --burn-cap 1

tezos-client --addr 178.62.73.193 get script storage for aliceLimit


# has tezos gold
# add liquidity
# %addLiquidity pair (pair (address %0) (nat %1)) (pair (nat %2) (timestamp %3))
# KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4

tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4"; CONTRACT %addLiquidity (pair (pair address nat) (pair nat timestamp)); IF_SOME {PUSH mutez 1000000; PUSH timestamp "2020-12-30T18:00:00Z"; PUSH nat 100; PAIR; PUSH nat 1; PUSH address "KT1WpcWd6xfdNDTZwrEZJm1VznC3GiGF6ZZN"; PAIR; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}'
```


Convert contract to JSON representation. Requires manually clean up.

```bash
tezos-client --addr 178.62.73.193 -l typecheck script spending_limit.tz > spending_limit.json 2>&1
```
