// ligo compile-contract spending_limit_reference.ligo main
// ligo compile-contract spending_limit_reference.ligo main > spending_limit_reference_raw.tz

type action is
| Do of (option(tez) -> operation)
| CheckLimit of tez
| Default of unit
| SetLimit of option(tez)

type storage is record
  last_transaction: option(timestamp * tez); // the tez is an accumulation spent since the last timestamp
  limit: option(tez);
  owner: key_hash;
end

type return is (list(operation) * storage);

const noLimit: option(tez) = None;

function do_(const f : option(tez) -> operation;
             const storage: storage): return is
 block {
   // the operations
   var op_list: list(operation) := nil;

   // do not allow tezo to be added
   if (amount > 0mutez) then block {
     failwith("1");
   } else skip;

   // make sure only the owner calls this contract
   if (sender = address(implicit_account(storage.owner))) then block {
     skip
   } else block {
     failwith("2");
   };

   // call the lambda f
   case (storage.limit) of
     | None -> block {
       const op1: operation = f(noLimit);
       op_list := list op1; end; 
     }
     | Some(limit) -> block {
       const op1: operation = f(Some(limit));
       
       // make sure we can transfer at least the expected remainder
       // this is broken in Michelson
       const self_contract: contract(action) = get_contract(self_address);

       // we don't know if f actually respects the limit, so we need to check it
       // check if the amount spent is within the allowed limit
       const op2: operation = transaction(CheckLimit(balance), 0mutez, self_contract);

       op_list := list op1; op2; end;
     }
   end;

 } with (op_list, storage);

const no_ops: list(operation) = nil;

function check_limit(const prev_balance: tez; var storage: storage): return is
  block {
    // can only be called by itself
    // make sure only limit calls itself
    if (sender = self_address) then block {
      skip
    } else block {
      failwith("3");
    };

    // how much was just spent
    // prev_balance should always be geq to balance
    const spent : tez = prev_balance - balance;

    var limit: tez := 0mutez;
    case (storage.limit) of
      | None -> skip
      | Some(l) -> block {limit := l}
    end;

    // edge case, spend zero and limit is zero, should pass
    if (spent = 0mutez and limit = 0mutez) then skip else block {
      // check how much was spent in last 24 hours
      case (storage.last_transaction) of
        | None -> block {
          if (spent > limit) then block {
            failwith("4")
          } else {
            // the user has no previous transaction
            // we know that this is in a fresh rolling window
            // so we record how much was spent in this transaction
            storage.last_transaction := Some(now, spent);
          }
        }
        | Some(xs) -> {
          // spend limit is how much one can spend in 24 hours
          if (now - xs.0 < 86400) then block {
            // 24 hours have not passed
            // spent plus the accumulated amount spent cannot surpass the limit
            if (spent + xs.1 > limit) then block {
              failwith("5");
            } else block {
              storage.last_transaction := Some(xs.0, xs.1 + spent);
            };
          } else block {
            // 24 hours have passed and they are spending part of the limit
            // reset the 24 hours
            if (spent > limit) then block {
              failwith("6");
            } else block {
              storage.last_transaction := Some(now, spent);
            }
          };
        }     
      end;
    }
  } with (no_ops, storage);

// this is just to send XTZ to this contract
function default_(const storage: storage): return is
  block {
    const op_list: list(operation) = nil;
  } with (op_list, storage);

const no_transaction : option(timestamp * tez) = None;

function setLimit(const limit : option(tez); var storage: storage): return is
  block {
    if (sender = address(implicit_account(storage.owner))) then block {
      skip
    } else block {
      failwith("7");
    };

    storage.last_transaction := no_transaction;
    storage.limit := limit;
    
  } with (no_ops, storage);

function main (const action : action ; const storage : storage) : (list(operation) * storage) is
  (case action of
  | Do(f)           -> do_(f, storage)
  | CheckLimit(prev_balance) -> check_limit(prev_balance, storage)
  | Default(xs)     -> default_(storage)
  | SetLimit(limit) -> setLimit(limit, storage)
  end);
