# Smartphone Integration of Phoenix Contracts

## spending_limit.tz

Spending limit is a Tezos contract that will be originated on the blockchain
during the on boarding process of the Phoenix wallet. The Phoenix user will have
a local TZ1 address (referred to as TZ1 for the rest of the documentation) that 
will be associated with the spending limit contract. The originated 
spending limit is a KT1 address (referred to as TZ1 or the rest of the documentation). 
It will not be called directly, rather it will be operated via the user's TZ1. 
KT1 is responsible for holding most of the XTZ and the FA1.2. TZ1 still needs an amount
of XTZ in order to operate KT1.

## Originate spending_limit.tz

`alice` is a TZ1 and her KT1 is `aliceLimit`. `alice`'s public address is
`tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn`. Change these values as needed.
You can an address from the command line with the following command:

```bash
tezos-client --addr 178.62.73.193 list known contracts | grep aliceLimit
aliceLimit: KT1Etn69GyqDCAhZp7wY21s4Y8HefH2ujrSf
```

```bash
tezos-client --addr 178.62.73.193 list known contracts | grep tezosGoldExchange
tezosGoldExchange: KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4
```

### Command line

```bash
tezos-client --addr 178.62.73.193 originate contract aliceLimit transferring 0 from alice running ./spending_limit.tz --init 'Pair (Pair None None) "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' --burn-cap 5 --force --dry-run
```

### JSON with string interpolation

- `limit`: the amount of XTZ that TZ1 is allowed to spend every 24 hours. Must be `None` or `Some(1000)` with a natural number.
- `owner`: the TZ1 address that will own this new KT1 contract.

```json
{
    "prim": "Pair",
    "args": [
        {
            "prim": "Pair",
            "args": [
                {
                    "prim": "None"
                },
                {
                    "prim": "None"
                }
            ]
        },
        {
            "string": "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"
        }
    ]
}
```

## Set Limit

Set the limit to 5 XTZ.

### Command line

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'setLimit' --arg 'Some 5000000'
```

### JSON with string interpolation

- `limit`: amount of xtz the owner can spend in a 24 hour window

```json
{
    "entrypoint": "setLimit",
    "value": {
        "prim": "Some",
        "args": [
            {
                "int": "$limit"
            }
        ]
    }
}
```

## Remove limit

Remove spending limit.

### Command line

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'setLimit' --arg 'None'
```

#### JSON with string interpolation

```json
{
    "entrypoint": "setLimit",
    "value": {
        "prim": "None"
    }
}
```

## Transfer KT1's XTZ via TZ1

Try testing out the limit and see if it is respected.

### Command line

Send one XTZ from aliceLimit to alice.

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"; CONTRACT unit; IF_SOME {PUSH mutez 1000000; UNIT; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;};}' --burn-cap 1
```

### JSON with tring interpolation

- `to_address`: the address that will receive the XTZ.
- `amount`: the amount of mutez to send to to_address.

```json
{
    "entrypoint": "do",
    "value": [
        {
            "prim": "DROP"
        },
        {
            "prim": "PUSH",
            "args": [
                {
                    "prim": "address"
                },
                {
                    "string": "$to_address"
                }
            ]
        },
        {
            "prim": "CONTRACT",
            "args": [
                {
                    "prim": "unit"
                }
            ]
        },
        [
            {
                "prim": "IF_NONE",
                "args": [
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "string"
                                },
                                {
                                    "string": ""
                                }
                            ]
                        },
                        {
                            "prim": "FAILWITH"
                        }
                    ],
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "mutez"
                                },
                                {
                                    "int": "$amount"
                                }
                            ]
                        },
                        {
                            "prim": "UNIT"
                        },
                        {
                            "prim": "TRANSFER_TOKENS"
                        }
                    ]
                ]
            }
        ]
    ]
}
```

## Transfer KT1's FA1.2 via TZ1

The limit contract does not know about its FA1.2 ownership and says nothing 
about how much FA1.2 it spends. Permissiona are handled directly by the FA1.2 
contract. FA1.2 controls its permissions. This interesting because usually 
KT1 would have to give TZ1 explicit permission, but by TZ1 calling FA1.2 via
KT1, it has permission via proxy to spend its tokens.

First get the address of the FA1.2 exchange:

```bash
tezos-client --addr 178.62.73.193 list known contracts | grep tezosGold
KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2
```

### Command line

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2"; CONTRACT %transfer (pair (pair address address) nat); IF_SOME {PUSH mutez 0; PUSH nat 100; PUSH address "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"; PUSH address "KT1Etn69GyqDCAhZp7wY21s4Y8HefH2ujrSf"; PAIR; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}' --dry-run
```

### String interpolation

The FA1.2 `tokens` are sent from the `kt1_address` to the `to_address`.

- `token_address`: the address of the FA1.2 tokens you want to send.
- `tokens`: the amount of tokens you want to send.
- `to_address`: the address that will receive the tokens.
- `kt1_address`: the address of the KT1 contract that TZ1 owns and will send the tokens.

```json
{
    "entrypoint": "do",
    "value": [
        {
            "prim": "DROP"
        },
        {
            "prim": "PUSH",
            "args": [
                {
                    "prim": "address"
                },
                {
                    "string": "$token_address"
                }
            ]
        },
        {
            "prim": "CONTRACT",
            "args": [
                {
                    "prim": "pair",
                    "args": [
                        {
                            "prim": "pair",
                            "args": [
                                {
                                    "prim": "address"
                                },
                                {
                                    "prim": "address"
                                }
                            ]
                        },
                        {
                            "prim": "nat"
                        }
                    ]
                }
            ],
            "annots": [
                "%transfer"
            ]
        },
        [
            {
                "prim": "IF_NONE",
                "args": [
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "string"
                                },
                                {
                                    "string": ""
                                }
                            ]
                        },
                        {
                            "prim": "FAILWITH"
                        }
                    ],
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "mutez"
                                },
                                {
                                    "int": "0"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "nat"
                                },
                                {
                                    "int": "$tokens"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "address"
                                },
                                {
                                    "string": "$to_address"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "address"
                                },
                                {
                                    "string": "$kt1_address"
                                }
                            ]
                        },
                        {
                            "prim": "PAIR"
                        },
                        {
                            "prim": "PAIR"
                        },
                        {
                            "prim": "TRANSFER_TOKENS"
                        }
                    ]
                ]
            }
        ]
    ]
}
```

## Dexter Entry Points

Unfortunately the Dexter entry points are gas intensive. In the future we will
optimize these, but for now we will use a work around. The basic idea is that
the XTZ, FA1.2 and Dexter Liquidity will belong to KT1, but KT1 will give 
TZ1 permission to spend this three tokens. This way, we can do smaller entry 
point calls without exceeding the allowed gas.

## XTZ to Token

### Send KT1 XTZ to TZ1

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; SOURCE; CONTRACT unit; IF_SOME {PUSH mutez 2000000; UNIT; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;};}' --burn-cap 1
```

### String interpolation

- `xtz_amount`: amount to give to KT1 to add liquidity on

```json
{
    "entrypoint": "do",
    "value": [
        {
            "prim": "DROP"
        },
        {
            "prim": "SOURCE"
        },
        {
            "prim": "CONTRACT",
            "args": [
                {
                    "prim": "unit"
                }
            ]
        },
        [
            {
                "prim": "IF_NONE",
                "args": [
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "string"
                                },
                                {
                                    "string": ""
                                }
                            ]
                        },
                        {
                            "prim": "FAILWITH"
                        }
                    ],
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "mutez"
                                },
                                {
                                    "int": "$xtz_amount"
                                }
                            ]
                        },
                        {
                            "prim": "UNIT"
                        },
                        {
                            "prim": "TRANSFER_TOKENS"
                        }
                    ]
                ]
            }
        ]
    ]
}
```

### TZ1 sells KT1's XTZ to Dexter and sends it to FA1.2

```bash
tezos-client --addr 178.62.73.193 transfer 2 from alice to tezosGoldExchange --entrypoint 'xtzToToken' --arg 'Pair (Pair "KT1Etn69GyqDCAhZp7wY21s4Y8HefH2ujrSf" 1) "2030-01-01T18:00:00Z"' --burn-cap 1
```

#### String interpolation

- `to_address`: this should be the KT1
- `min-tokens-required`: minimum amount of token
- `deadline`: timestamp for period in which this transaction is valid

```json
{
    "entrypoint": "xtzToToken",
    "value": {
        "prim": "Pair",
        "args": [
            {
                "prim": "Pair",
                "args": [
                    {
                        "string": "$to_address"
                    },
                    {
                        "int": "$min-tokens-required"
                    }
                ]
            },
            {
                "string": "$deadline"
            }
        ]
    }
}
```

## Token to XTZ

### Approve TZ1 to spend KT1's FA1.2

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2"; CONTRACT %approve (pair address nat); IF_SOME {PUSH mutez 0; PUSH nat 1000000; PUSH address "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}' --burn-cap 1
```

#### JSON with string interpolation

- `token_address`: the FA1.2 token
- `allowance`: amount that tz1_address will be allowed to send/sell
- `tz1_address`: the TZ1 that owns KT1


```json
{
    "entrypoint": "do",
    "value": [
        {
            "prim": "DROP"
        },
        {
            "prim": "PUSH",
            "args": [
                {
                    "prim": "address"
                },
                {
                    "string": "$token_address"
                }
            ]
        },
        {
            "prim": "CONTRACT",
            "args": [
                {
                    "prim": "pair",
                    "args": [
                        {
                            "prim": "address"
                        },
                        {
                            "prim": "nat"
                        }
                    ]
                }
            ],
            "annots": [
                "%approve"
            ]
        },
        [
            {
                "prim": "IF_NONE",
                "args": [
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "string"
                                },
                                {
                                    "string": ""
                                }
                            ]
                        },
                        {
                            "prim": "FAILWITH"
                        }
                    ],
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "mutez"
                                },
                                {
                                    "int": "0"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "nat"
                                },
                                {
                                    "int": "$allowance"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "address"
                                },
                                {
                                    "string": "$tz1_address"
                                }
                            ]
                        },
                        {
                            "prim": "PAIR"
                        },
                        {
                            "prim": "TRANSFER_TOKENS"
                        }
                    ]
                ]
            }
        ]
    ]
}
```


### TZ1 sells KT1's FA1.2 to Dexter and sends it to KT1

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to tezosGoldExchange --entrypoint 'tokenToXtz' --arg 'Pair (Pair (Pair "KT1Etn69GyqDCAhZp7wY21s4Y8HefH2ujrSf" "KT1Etn69GyqDCAhZp7wY21s4Y8HefH2ujrSf") (Pair 50 1000000)) "2030-01-01T18:00:00Z"' --burn-cap 1
```

#### JSON with string interpolation

- `owner_address`: this should be the KT1
- `to_address`: this should be the KT1
- `tokens_sold`: amount of tokens that the user wants to sell
- `min_xtz_bought`: minimum amount of XTZ that the user wants to receive
- `deadline`: timestamp for period in which this transaction is valid



```json
{
    "entrypoint": "tokenToXtz",
    "value": {
        "prim": "Pair",
        "args": [
            {
                "prim": "Pair",
                "args": [
                    {
                        "prim": "Pair",
                        "args": [
                            {
                                "string": "$owner_address"
                            },
                            {
                                "string": "$to_address"
                            }
                        ]
                    },
                    {
                        "prim": "Pair",
                        "args": [
                            {
                                "int": "$tokens_sold"
                            },
                            {
                                "int": "$min_xtz_bought"
                            }
                        ]
                    }
                ]
            },
            {
                "string": "$deadline"
            }
        ]
    }
}
```

## Add Liquidity

Add liquidity requires that Dexter has permission to spend KT1's FA1.2 and 
transfer KT1's XTZ to TZ1.

### Approve Dexter exchange to spend KT1's FA1.2

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1HWN9KL97fo7oYZgy36Je7ZYZAY6Xt4ye2"; CONTRACT %approve (pair address nat); IF_SOME {PUSH mutez 0; PUSH nat 1000000; PUSH address "KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4"; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}' --burn-cap 1
```

#### JSON with string interpolation

- `token_address`: the FA1.2 token
- `allowance`: amount that tz1_address will be allowed to send/sell
- `dexter_address`: the Dexter that TZ1 will sell to

```json
{
    "entrypoint": "do",
    "value": [
        {
            "prim": "DROP"
        },
        {
            "prim": "PUSH",
            "args": [
                {
                    "prim": "address"
                },
                {
                    "string": "$token_address"
                }
            ]
        },
        {
            "prim": "CONTRACT",
            "args": [
                {
                    "prim": "pair",
                    "args": [
                        {
                            "prim": "address"
                        },
                        {
                            "prim": "nat"
                        }
                    ]
                }
            ],
            "annots": [
                "%approve"
            ]
        },
        [
            {
                "prim": "IF_NONE",
                "args": [
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "string"
                                },
                                {
                                    "string": ""
                                }
                            ]
                        },
                        {
                            "prim": "FAILWITH"
                        }
                    ],
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "mutez"
                                },
                                {
                                    "int": "0"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "nat"
                                },
                                {
                                    "int": "$allowance"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "address"
                                },
                                {
                                    "string": "$dexter_address"
                                }
                            ]
                        },
                        {
                            "prim": "PAIR"
                        },
                        {
                            "prim": "TRANSFER_TOKENS"
                        }
                    ]
                ]
            }
        ]
    ]
}
```



### Send KT1 XTZ to TZ1

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; SOURCE; CONTRACT unit; IF_SOME {PUSH mutez 2000000; UNIT; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;};}' --burn-cap 1
```

#### JSON with dtring interpolation

- `xtz_amount`: amount to give to KT1 to add liquidity on

```json
{
    "entrypoint": "do",
    "value": [
        {
            "prim": "DROP"
        },
        {
            "prim": "SOURCE"
        },
        {
            "prim": "CONTRACT",
            "args": [
                {
                    "prim": "unit"
                }
            ]
        },
        [
            {
                "prim": "IF_NONE",
                "args": [
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "string"
                                },
                                {
                                    "string": ""
                                }
                            ]
                        },
                        {
                            "prim": "FAILWITH"
                        }
                    ],
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "mutez"
                                },
                                {
                                    "int": "$xtz_amount"
                                }
                            ]
                        },
                        {
                            "prim": "UNIT"
                        },
                        {
                            "prim": "TRANSFER_TOKENS"
                        }
                    ]
                ]
            }
        ]
    ]
}
```

### TZ1 adds liquidity for KT1

```bash
tezos-client --addr 178.62.73.193 transfer 2 from alice to tezosGoldExchange --entrypoint 'addLiquidity' --arg 'Pair (Pair "KT1Etn69GyqDCAhZp7wY21s4Y8HefH2ujrSf" 1) (Pair 100 "2030-01-01T18:00:00Z")' --burn-cap 1
```
#### JSON with string interpolation

- `owner_address`: the KT1 address that will own this
- `min_lqt_created`: the minimum amount of Dexter liquidity created
- `max_tokens_deposited`: the maximum number of tokens to send
- `deadline`: timestamp for period in which this transaction is valid

```
{
    "entrypoint": "addLiquidity",
    "value": {
        "prim": "Pair",
        "args": [
            {
                "prim": "Pair",
                "args": [
                    {
                        "string": "$owner_address"
                    },
                    {
                        "int": "$min_lqt_created"
                    }
                ]
            },
            {
                "prim": "Pair",
                "args": [
                    {
                        "int": "$max_tokens_deposited"
                    },
                    {
                        "string": "$deadline"
                    }
                ]
            }
        ]
    }
}
```

## Remove Liquidty

### Approve TZ1 to burn KT1's Dexter liquidity

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH address "KT1Var2egEQtc58fCVDpsNJdyzuWZ8yacvW4"; CONTRACT %approve (pair (pair address nat) nat); IF_SOME {PUSH mutez 0; PUSH nat 0; PUSH nat 10000000000; PUSH address "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"; PAIR; PAIR; TRANSFER_TOKENS;} {PUSH string ""; FAILWITH;}}' --burn-cap 1
```

#### JSON with string interpolation

- `dexter_address`: the Dexter that TZ1 will sell to
- `allowance`: how much liquidity they want to burn
- `current_allowance`: the current amount in storage, this is a security measure
- `spender`: the TZ1 address

```json
{
    "entrypoint": "do",
    "value": [
        {
            "prim": "DROP"
        },
        {
            "prim": "PUSH",
            "args": [
                {
                    "prim": "address"
                },
                {
                    "string": "$dexter_address"
                }
            ]
        },
        {
            "prim": "CONTRACT",
            "args": [
                {
                    "prim": "pair",
                    "args": [
                        {
                            "prim": "pair",
                            "args": [
                                {
                                    "prim": "address"
                                },
                                {
                                    "prim": "nat"
                                }
                            ]
                        },
                        {
                            "prim": "nat"
                        }
                    ]
                }
            ],
            "annots": [
                "%approve"
            ]
        },
        [
            {
                "prim": "IF_NONE",
                "args": [
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "string"
                                },
                                {
                                    "string": ""
                                }
                            ]
                        },
                        {
                            "prim": "FAILWITH"
                        }
                    ],
                    [
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "mutez"
                                },
                                {
                                    "int": "0"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "nat"
                                },
                                {
                                    "int": "$current_allowance"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "nat"
                                },
                                {
                                    "int": "$allowance"
                                }
                            ]
                        },
                        {
                            "prim": "PUSH",
                            "args": [
                                {
                                    "prim": "address"
                                },
                                {
                                    "string": "$spender"
                                }
                            ]
                        },
                        {
                            "prim": "PAIR"
                        },
                        {
                            "prim": "PAIR"
                        },
                        {
                            "prim": "TRANSFER_TOKENS"
                        }
                    ]
                ]
            }
        ]
    ]
}
```



### TZ1 burns KT1's Dexter liquidity

```bash
tezos-client --addr 178.62.73.193 transfer 0 from alice to tezosGoldExchange --entrypoint 'removeLiquidity' --arg 'Pair (Pair (Pair "KT1Etn69GyqDCAhZp7wY21s4Y8HefH2ujrSf" "KT1Etn69GyqDCAhZp7wY21s4Y8HefH2ujrSf") (Pair 1000000 1)) (Pair 1 "2020-06-29T18:00:21Z")' --burn-cap 1
```

#### JSON with string interpolation

- `owner_address`: the KT1 address owns the liquidity
- `to_address`: the KT1 address
- `lqt_burned`: the amount of liquidity to burn (TZ1 needs permission)
- `min_xtz_withdrawn`: minimum amount of XTZ to receive for burning
- `min_tokens_withdrawn`: minimum amount of FA1.2 to receive for burning
- `deadline`: timestamp for period in which this transaction is valid

```json
{
    "entrypoint": "removeLiquidity",
    "value": {
        "prim": "Pair",
        "args": [
            {
                "prim": "Pair",
                "args": [
                    {
                        "prim": "Pair",
                        "args": [
                            {
                                "string": "$owner_address"
                            },
                            {
                                "string": "$to_address"
                            }
                        ]
                    },
                    {
                        "prim": "Pair",
                        "args": [
                            {
                                "int": "$lqt_burned"
                            },
                            {
                                "int": "$min_xtz_withdrawn"
                            }
                        ]
                    }
                ]
            },
            {
                "prim": "Pair",
                "args": [
                    {
                        "int": "$min_tokens_withdrawn"
                    },
                    {
                        "string": "$deadline"
                    }
                ]
            }
        ]
    }
}
```

## Set Delegate (Baker)

```
tezos-client --addr 178.62.73.193 transfer 0 from alice to aliceLimit --entrypoint 'do' --arg '{DROP; PUSH key_hash "tz1PirboZKFVqkfE45hVLpkpXaZtLk3mqC17"; SOME; SET_DELEGATE;}' --burn-cap 1
```

### JSON with string interpolation

- `baker`: key_hash of baker

```json
{
    "entrypoint": "do",
    "value": [
        {
            "prim": "DROP"
        },
        {
            "prim": "PUSH",
            "args": [
                {
                    "prim": "key_hash"
                },
                {
                    "string": "$baker"
                }
            ]
        },
        {
            "prim": "SOME"
        },
        {
            "prim": "SET_DELEGATE"
        }
    ]
}
```
