# Spending Limit: An Informal Specification

A spending limit contract holds a user's XTZ and allows them to set a spending
limit. Once any amount of XYZ is spent, a 24 hour period begins and the limit 
is in affect until 24 hours have passed. The owner can only spend a cumulative
amount that is less than or equal to the limit during the 24 hour period. Once 
24 hours have passed, the cumulative amount spent will be zero and the user can
spend an amount up to the limit in a new 24 hour period.

# Terminology

- xtz: the official token on the Tezos blockchain. Also known as tez or 
colloquially as tezzies. The smallest unit is mutez. The common unit is tez. 1 
tez = 1000000 mutez. For clarity, we will refer to this token as XTZ.

- lambda: a function that can be treated as a parameter and passed to an entry 
point.

- sender: the contract that initiated the current internal transaction, as 
returned by SENDER in Michelson. Allowed senders are the owner of the contract
and the contract itself.

# Storage

- last_transaction (option: (last_spenditure: timestamp, accumulated_expenses: 
xtz)): 

- limit (option: xtz): when None there is no limit. When Some it is the maximum
amount the owner can spend during one 24 period.

- owner (key_hash): the owner of the limit contract. Only the owner can spend 
this contract's xtz.

# Entry Points

- default
- set limit
- do
- check limit




## Default

This is the entry point we call to give xtz to the set limit contract.

### Parameters

- unit: the default parameter. It carries no data.

### Logic

This does nothing but receive xtz.

### Operations

- no operations

### Error

- no errors




## Set Limit

Update the limit of the spending limit contract.

### Parameters

- new_limit option(xtz): None means no spending limit, Some contains a non-zero limit.

### Logic

Assert that sender is the owner of this spending limit contract.

    last_transaction := None

    limit := new_limit

### Operations

- no operations

### Errors

- sender is not the owner.




## Do

Do allows the owner to perform a lambda f, using the xtz of the spending limit 
contract that respects the spending limit. f could attempt to spend more than
the limit so there is a secondary entry point that do calls to check this. If
the limit is violated, the call to this entry point will fail.

### Parameters

- f (option(xtz) -> operation): A function that takes the spending limit and 
returns a single operation.

### Logic

Assert that the sender is the owner.

If there is no limit, then execute lambda f with None.

If there is a limit, pass it to lambda f, execute it, then pass the balance from
before executing lambda f to a transaction call to Check Limit.

### Operations

- exec lambda f
- call check limit entry pointt

### Errors

- sender is not the owner of the contract.
- sender tries to send xtz. They should use Default for this.
- amount spend by lambda f exceeds the spending limit.




## Check Limit

The sole purpose of this entrypoint is to check that the lambda called in Do 
respects the spending limit. It does this via a parameter that records the
balance before calling lambda f, compares it to the balance after calling 
lambda f, and asserting that the difference between them is less than or
equal to the spending limit.

### Parameters

- prev_balance xtz: the balance before calling lambda f in the Do entrypoint.

### Logic

Assert that the sender is the contract itself.

Calculate the amount spent.

    spent := prev_balance - balance

If the amount spent is zero, then there is nothing else to do because we know 
lambda f has respected the limit.

If last_transaction.last_spenditure is less than 24 hours, assert that

    last_transaction.accumulated_expenses + spent <= limit

then set

    last_transaction.accumulated_expenses := last_transaction.accumulated_expenses + spent

If last_transaction.last_spenditure is greater than 24 hours, than the 24 
withdrawal window has passed and it can ignore 
last_transaction.accumulated_expenses.

Assert that

    last_transaction.accumulated_expenses <= limit

then set

    last_transaction.accumulated_expenses := spent

    last_transaction.last_spenditure := now

### Operations

- no operations

### Errors

- the sender is not the contract.
- lambda f tried to spend more than the limit.
