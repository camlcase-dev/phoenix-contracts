# Changelog
All notable changes to phoenix-contracts will be documented in this file.

## Beta

## 0.1.6 -- 2020-03-12
* [spending_limit_informal_specification.md] Start an informal specification of how the spending limit contract should behave.
* Reorganize files into contracts and docs directories.
* Update README to include details about the project intent and structure.

## 0.1.5 -- 2020-03-10
* [smartphone_integration.md] Produce Micheline JSON parameters for all the entry points that need to be called.

## 0.1.4 -- 2020-03-10
* [spending_limit.tz] Fix bug that reversed the spending limit (it required you to spend more than the limit).

* [spending_limit.tz] Fix bugs that miscalculated the accumulated expenses during a 24 hour window.

## 0.1.3 -- 2020-02-28
* [smartphone_integration.md] Add kotlin and swift headers for clarity.

## 0.1.2 -- 2020-02-28
* [smartphone_integration.md] Details for XTZ to Token, Add Liquidity and Remove Liquidity.

## 0.1.1 -- 2020-02-28
* [spending_limit.json] Commit the `spending_limit.json` file.

* [README.md] Document how to convert a `.tz` contract to `.json`

* [smartphone_integration.md] Add details how to perform Token to XTZ with the
  spending limit contract.

## 0.1.0 -- 2020-02-27
* [spending_limit_reference.ligo] Write the spending_limit_reference.ligo
  contract in PascalLigo. This contract is for experimenting and development.
  It is not intended for production use. Currently LIGO has an error with
  converting self_address to a contract so the code must be manually edited.

* [spending_limit.tz] Write the spending_limit.tz contract in Michelson. This
  is the production version of spending_limit_reference.ligo.

* [smartphone_integration.md] Write smartphone_integration.md which documents
  how to integrate the contracts into the phoenix smart phone applications.

* [spending_limit_reference_scratch.md,spending_limit_scratch.md] Include
  *_scratch.md files which are useful for development of the contracts.
