# phoenix-contracts

Tezos smart contracts for the phoenix smart wallet.

## Spending limit contract

This contract holds xtz for a single owner account. Only the owner is allowed to
spend the xtz held in this contract. It can also hold FA1.2 for the owner.

- [contracts/spending_limit_reference.ligo](contracts/spending_limit_reference.ligo) is a reference implementation of the spending limit contract. It is easy to read but the output code is not optimized.

- [contracts/spending_limit.tz](contracts/spending_limit.tz) is the contract used by phoenix. It is handwritten and optimized in Michelson. Every line of code is annotated with the stack value after running the command.

- [docs/smartphone_integration.md](docs/smartphone_integration.md) explains how to integrate the spending limit contract into the phoenix mobile applications.

- [docs/spending_limit_informal_specification.md](docs/spending_limit_informal_specification.md) formally describes what the contract does, the parameters it takes, the operations it creates and the errors that can occur. This is the starting point for a formalized version that would be written in Mi-Cho-Coq.

